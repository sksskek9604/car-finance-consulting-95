package com.amh.carfinanceconsulting.repository;

import com.amh.carfinanceconsulting.entity.CarTrim;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarTrimRepository extends JpaRepository<CarTrim, Long> {
    //차량의 트림들
    List<CarTrim> findAllByCarRating_CarModel_Id(Long modelId);
}
