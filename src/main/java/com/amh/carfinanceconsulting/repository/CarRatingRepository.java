package com.amh.carfinanceconsulting.repository;

import com.amh.carfinanceconsulting.entity.CarRating;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRatingRepository extends JpaRepository<CarRating, Long> {
}
