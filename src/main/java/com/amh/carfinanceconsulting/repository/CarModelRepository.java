package com.amh.carfinanceconsulting.repository;

import com.amh.carfinanceconsulting.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {
}
