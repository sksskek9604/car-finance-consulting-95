package com.amh.carfinanceconsulting.repository;

import com.amh.carfinanceconsulting.entity.ConsultationDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultationDetailsRepository extends JpaRepository<ConsultationDetails, Long> {
}
