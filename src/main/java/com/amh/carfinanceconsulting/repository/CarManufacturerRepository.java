package com.amh.carfinanceconsulting.repository;

import com.amh.carfinanceconsulting.entity.CarManufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarManufacturerRepository extends JpaRepository<CarManufacturer, Long> {
}
