package com.amh.carfinanceconsulting.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
