package com.amh.carfinanceconsulting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarFinanceConsultingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarFinanceConsultingApplication.class, args);
    }

}
