package com.amh.carfinanceconsulting.service;

import com.amh.carfinanceconsulting.entity.CarManufacturer;
import com.amh.carfinanceconsulting.entity.CarModel;
import com.amh.carfinanceconsulting.entity.CarRating;
import com.amh.carfinanceconsulting.entity.CarTrim;
import com.amh.carfinanceconsulting.exception.CMissingDataException;
import com.amh.carfinanceconsulting.model.CarInfoDetailResponse;
import com.amh.carfinanceconsulting.model.CarListItem;
import com.amh.carfinanceconsulting.model.car_Trim.CarTrimCreateRequest;
import com.amh.carfinanceconsulting.model.car_manufacturer.CarManufacturerRequest;
import com.amh.carfinanceconsulting.model.car_model.CarManuIdRequest;
import com.amh.carfinanceconsulting.model.car_model.CarModelCreateRequest;
import com.amh.carfinanceconsulting.model.car_rating.CarRatingCreateRequest;
import com.amh.carfinanceconsulting.model.common.ListResult;
import com.amh.carfinanceconsulting.repository.CarManufacturerRepository;
import com.amh.carfinanceconsulting.repository.CarModelRepository;
import com.amh.carfinanceconsulting.repository.CarRatingRepository;
import com.amh.carfinanceconsulting.repository.CarTrimRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
@RequiredArgsConstructor
public class CarInfoService {
    private final CarManufacturerRepository carManufacturerRepository;
    private final CarModelRepository carModelRepository;
    private final CarRatingRepository carRatingRepository;
    private final CarTrimRepository carTrimRepository;

    public void setCarManufacturer(CarManufacturerRequest request) {
        CarManufacturer carManufacturer = new CarManufacturer.CarManufacturerBuilder(request).build();
        carManufacturerRepository.save(carManufacturer); // 자동차 제조사명 C
    }
    public CarManufacturer getCarManufacturerData(long id) {
        return carManufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
    // Manufacturer


    public CarModel getCarModelData(long modelId) {
        return carModelRepository.findById(modelId).orElseThrow(CMissingDataException::new);
    }
    public void setCarModel(CarManufacturer carManufacturer, CarModelCreateRequest request) {
        CarModel carModel = new CarModel.CarModelBuilder(carManufacturer, request).build();
        carModelRepository.save(carModel); // 자동차 모델명 C
    }
   /* public void putCarModelManuId(long id, CarManufacturer carManufacturer, CarManuIdRequest request) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
        carModel.putCarManufacturerIdInCarModel(carManufacturer, request);
        carModelRepository.save(carModel);
    }
    // CarModel*/

    public CarRating getCarRatingData(long ratingId) {
        return carRatingRepository.findById(ratingId).orElseThrow(CMissingDataException::new);
    }
    public void setCarRating(CarModel carModel, CarRatingCreateRequest request) {
        CarRating carRating = new CarRating.CarRatingBuilder(carModel, request).build();
        carRatingRepository.save(carRating); // 자동차 등급 C
    }
    // CarRating


    public CarTrim getCarTrimData(long carTrimId) {
        return carTrimRepository.findById(carTrimId).orElseThrow(CMissingDataException::new);
    }
    public void setCarTrim(CarRating carRating, CarTrimCreateRequest request) {
        CarTrim carTrim = new CarTrim.CarTrimBuilder(carRating, request).build();
        carTrimRepository.save(carTrim); // 자동차 트림 C
    }
    public ListResult<CarListItem> getCarList() {
        List<CarModel> carModels = carModelRepository.findAll();

        List<CarListItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarListItem addItem = new CarListItem.CarListItemBuilder(carModel).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    } // 다음 자동차 화면에 간략히 보이는 정보

    public CarInfoDetailResponse getCarInfoDetail(long modelId) {
        CarModel carModel = carModelRepository.findById(modelId).orElseThrow(CMissingDataException::new);
        //트림 기준으로 가져와야하는 리스트, 차량 모델이 carmodel인 트림들만 가져오면 된다.
        List<CarTrim> trims = carTrimRepository.findAllByCarRating_CarModel_Id(carModel.getId());

        ArrayList<Double> prices = new ArrayList<>(); // 금액대만 뽑은 배열만들기
        trims.forEach(trim -> {
            prices.add(trim.getCarPrice());
        });

        Collections.sort(prices);

        Double maxPrice = prices.get(0); // 맨첫번째 있으니까 0번째꺼 가져오고
        Double minPrice = prices.get(prices.size() - 1); // 늘 마지막에 있을 테니, 사이즈가 5개면 5개지만, 0~4로 치니까 -1
        //

        List<String> carFuelTypes = new LinkedList<>();
        trims.forEach(trim -> {
            carFuelTypes.add(trim.getTypeFuel().getName());
        });

        Set<String> tempSet = new HashSet<>(carFuelTypes);
        // 리스트를 셋으로 변환하는 순간 셋은 중복을 허용하지 않아서, 중복된게 하나로 합쳐짐
        List<String> carFuelTypesResult = new LinkedList<>(tempSet);

        ArrayList<Integer> displacement = new ArrayList<>();
        trims.forEach(trim -> {
            displacement.add(trim.getDisplacement());
        });

        Collections.sort(displacement);

        Integer maxDisplacement = displacement.get(0);
        Integer minDisplacement = displacement.get(prices.size() - 1);

        ArrayList<Double> carMileage = new ArrayList<>();
        trims.forEach(trim -> {
            carMileage.add(trim.getMileageCombined()); // getCombmileage라고
        });

        Collections.sort(prices);

        Double minCarMileage = carMileage.get(0);
        Double maxCarMileage = carMileage.get(carMileage.size() - 1);


        ArrayList<Integer> carCapacity = new ArrayList<>();
        trims.forEach(trim -> {
            carCapacity.add(trim.getMaximumPeople()); // getCarCapacity라고
        });

        Collections.sort(carCapacity);

        Integer minCarCapacity = carCapacity.get(0);
        Integer maxCarCapacity = carCapacity.get(carCapacity.size() - 1);

        return new CarInfoDetailResponse.CarInfoDetailResponseBuilder(
                carModel,
                minPrice,
                maxPrice,
                carFuelTypesResult,
                minDisplacement,
                maxDisplacement,
                minCarMileage,
                maxCarMileage,
                minCarCapacity,
                maxCarCapacity
        ).build();
    }
    // 차 모델: , 가격: (min~max), 연료:, 연비: min 0.0 ~ max 0.0, 외장: , 배기량 min 0000~ 0000, 정원:
}
