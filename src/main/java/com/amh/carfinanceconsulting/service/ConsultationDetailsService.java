package com.amh.carfinanceconsulting.service;

import com.amh.carfinanceconsulting.entity.CarTrim;
import com.amh.carfinanceconsulting.entity.ConsultationDetails;
import com.amh.carfinanceconsulting.model.consultationDetails.ConsultationDetailsCreateRequest;
import com.amh.carfinanceconsulting.repository.ConsultationDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConsultationDetailsService {

    private final ConsultationDetailsRepository consultationDetailsRepository;
    public void setConsultationDetails(CarTrim carTrim, ConsultationDetailsCreateRequest request) {
        ConsultationDetails consultationDetails = new ConsultationDetails.ConsultationDetailsBuilder(carTrim, request).build();
        consultationDetailsRepository.save(consultationDetails);
    }

}
