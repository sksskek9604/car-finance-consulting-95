package com.amh.carfinanceconsulting.controller;

import com.amh.carfinanceconsulting.entity.CarTrim;
import com.amh.carfinanceconsulting.model.common.CommonResult;
import com.amh.carfinanceconsulting.model.consultationDetails.ConsultationDetailsCreateRequest;
import com.amh.carfinanceconsulting.service.CarInfoService;
import com.amh.carfinanceconsulting.service.ConsultationDetailsService;
import com.amh.carfinanceconsulting.service.ResponseService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "1차 상담 내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/consult_detail")
public class ConsultationDetailsController {

    private final CarInfoService carInfoService;
    private final ConsultationDetailsService consultationDetailsService;

    @PostMapping("/new")
    public CommonResult setConsultationDetail(@RequestBody @Valid ConsultationDetailsCreateRequest request) {
        CarTrim carTrim = carInfoService.getCarTrimData(request.getCarTrimId());
        consultationDetailsService.setConsultationDetails(carTrim, request);
        return ResponseService.getSuccessResult();
    }
}
