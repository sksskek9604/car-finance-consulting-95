package com.amh.carfinanceconsulting.controller;

import com.amh.carfinanceconsulting.entity.CarManufacturer;
import com.amh.carfinanceconsulting.entity.CarModel;
import com.amh.carfinanceconsulting.entity.CarRating;
import com.amh.carfinanceconsulting.model.CarInfoDetailResponse;
import com.amh.carfinanceconsulting.model.CarListItem;
import com.amh.carfinanceconsulting.model.car_Trim.CarTrimCreateRequest;
import com.amh.carfinanceconsulting.model.car_manufacturer.CarManufacturerRequest;
import com.amh.carfinanceconsulting.model.car_model.CarManuIdRequest;
import com.amh.carfinanceconsulting.model.car_model.CarModelCreateRequest;
import com.amh.carfinanceconsulting.model.car_rating.CarRatingCreateRequest;
import com.amh.carfinanceconsulting.model.common.CommonResult;
import com.amh.carfinanceconsulting.model.common.ListResult;
import com.amh.carfinanceconsulting.model.common.SingleResult;
import com.amh.carfinanceconsulting.service.CarInfoService;
import com.amh.carfinanceconsulting.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car")
public class CarController {

    private final CarInfoService carInfoService;
    @ApiOperation(value = "차량 제조사 정보 등록")
    @PostMapping("/set-carManufacturer/new")
    public CommonResult setCarManufacturer(@RequestBody @Valid CarManufacturerRequest request) {
        carInfoService.setCarManufacturer(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 모델 정보 등록")
    @PostMapping("/carModel/new")
    public CommonResult setCarModel(@RequestBody @Valid CarModelCreateRequest request) {
        CarManufacturer carManufacturer = carInfoService.getCarManufacturerData(request.getCarManufacturerId());
        carInfoService.setCarModel(carManufacturer, request);

        return ResponseService.getSuccessResult();
    }

    /*@ApiOperation(value = "차량 모델 제조사 수정")
    @PutMapping("/carModel-manu-Id/{id}")
    public CommonResult putCarModelManuId(@PathVariable long id, @RequestBody @Valid CarManuIdRequest request) {
        carInfoService.putCarModelManuId(id, request);
        return ResponseService.getSuccessResult();
    } // put보류*/

    @ApiOperation(value = "차량 등급 정보 등록")
    @PostMapping("/carRating/new")
    public CommonResult setCarRating(@RequestBody @Valid CarRatingCreateRequest request) {
        CarModel carModel = carInfoService.getCarModelData(request.getCarModelId());
        carInfoService.setCarRating(carModel, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 트림 정보 등록")
    @PostMapping("/carTrim/new")
    public CommonResult setCarTrim(@RequestBody @Valid CarTrimCreateRequest request) {
        CarRating carRating = carInfoService.getCarRatingData(request.getCarRatingId());
        carInfoService.setCarTrim(carRating, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 리스트 불러오기")
    @GetMapping("/list")
    public ListResult<CarListItem> getCarList() {
        return ResponseService.getListResult(carInfoService.getCarList(), true);
    }

    @ApiOperation(value = "차량 상세정보 불러오기")
    @GetMapping("/model/{modelId}")
    public SingleResult<CarInfoDetailResponse> getCarInfoDetail(@PathVariable long modelId) {
        return ResponseService.getSingleResult(carInfoService.getCarInfoDetail(modelId));
    }

}
