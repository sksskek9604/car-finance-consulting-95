package com.amh.carfinanceconsulting.entity;

import com.amh.carfinanceconsulting.enums.car_trim.*;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import com.amh.carfinanceconsulting.model.car_Trim.CarTrimCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarTrim {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carRatingId", nullable = false)
    private CarRating carRating;

    @Column(nullable = false, length = 30)
    private String nameTrim;

    @Column(nullable = false)
    private Double carPrice; //가격, 만원단위니까 0도 수 만큼 많이 붙여야함

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private TypeFuel typeFuel;

    @Column(nullable = false, length = 50)
    private String typeEngin; //엔진형식

    @Column(nullable = false)
    private Integer displacement; //배기량

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 30)
    private TypeGearBox typeGearbox; //변속기 종류

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private TypeDrive typeDrive; // 구동 방식

    @Column(nullable = false)
    private Integer outputHighest; // 최고 출력

    @Column(nullable = false)
    private Double torqueMaximum; //최대 토크

    @Column(nullable = false)
    private Double mileageCombined; //복합연비

    @Column(nullable = false)
    private Double mileageCity; //도심연비

    @Column(nullable = false)
    private Double mileageHighway; //고속도로 연비

    @Column(nullable = false)
    private Short ratingMileage; //연비등급

    @Column(nullable = false)
    private Double emissionsCo2; // CO2배출량

    @Column(nullable = false)
    private Short gradeLowPollution; //저공해등급

    @Column(nullable = false)
    private Double weightCurb; //공차중량

    @Column(nullable = false)
    private Integer speedMaximum; //최고속도

    @Column(nullable = false)
    private Double zeroToSixty; //제로백

    @Column(nullable = false)
    private Short levelAutomation; //자율주행레벨

    @Column(nullable = false, length = 20)
    private String tireSizeFront; //앞 타이어 규격

    @Column(nullable = false, length = 20)
    private String tireSizeBack; // 뒷 타이어 규격

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private BrakeType frontBrake; // 전륜 브레이크

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private BrakeType backBrake; //후륜 브레이크

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private Suspension frontSuspensionType;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private Suspension backSuspensionType;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private PowerSteering powerSteering;

    @Column(nullable = false)
    private Integer maximumPeople; // 차량 정원

    // 전기차
    @Column(nullable = false)
    private Double fuelEfficiency; // 연비

    @Column(nullable = false)
    private Short chargingMileage; // 충전 주행거리

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private BatteryType batteryType; //배터리 종류

    @Column(nullable = false)
    private Double batteryCapacity; // 배터리 용량

    @Column(nullable = false)
    private String chargingWay; // 충전 방식

    @Column(nullable = false)
    private Short fastChargeTime; // 급속 충전시간

    @Column(nullable = false)
    private Short standardChargeTime; //완속 충전시간

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private CarTrim(CarTrimBuilder builder) {

        this.nameTrim = builder.nameTrim;
        this.carRating = builder.carRating;
        this.carPrice = builder.carPrice;
        this.typeFuel = builder.typeFuel;
        this.typeEngin = builder.typeEngin;
        this.displacement = builder.displacement;
        this.typeGearbox = builder.typeGearbox;
        this.typeDrive = builder.typeDrive;
        this.outputHighest = builder.outputHighest;
        this.torqueMaximum = builder.torqueMaximum;
        this.mileageCombined = builder.mileageCombined;
        this.mileageCity = builder.mileageCity;
        this.mileageHighway = builder.mileageHighway;
        this.ratingMileage = builder.ratingMileage;
        this.emissionsCo2 = builder.emissionsCo2;
        this.gradeLowPollution = builder.gradeLowPollution;
        this.weightCurb = builder.weightCurb;
        this.speedMaximum = builder.speedMaximum;
        this.zeroToSixty = builder.zeroToSixty;
        this.levelAutomation = builder.levelAutomation;
        this.tireSizeFront = builder.tireSizeFront;
        this.tireSizeBack = builder.tireSizeBack;
        this.frontBrake = builder.frontBrake;
        this.backBrake = builder.backBrake;
        this.frontSuspensionType = builder.frontSuspensionType;
        this.backSuspensionType = builder.backSuspensionType;
        this.powerSteering = builder.powerSteering;
        this.maximumPeople = builder.maximumPeople;
        this.fuelEfficiency = builder.fuelEfficiency;
        this.chargingMileage = builder.chargingMileage;
        this.batteryType = builder.batteryType;
        this.batteryCapacity = builder.batteryCapacity;
        this.chargingWay = builder.chargingWay;
        this.fastChargeTime = builder.fastChargeTime;
        this.standardChargeTime = builder.standardChargeTime;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarTrimBuilder implements CommonModelBuilder<CarTrim> {

        private final String nameTrim;
        private final CarRating carRating;
        private final Double carPrice; //가격, 만원단위니까 0도 수 만큼 많이 붙여야함
        private final TypeFuel typeFuel;
        private final String typeEngin; //엔진형식
        private final Integer displacement; //배기량
        private final TypeGearBox typeGearbox; //변속기 종류
        private final TypeDrive typeDrive; // 구동 방식
        private final Integer outputHighest; // 최고 출력
        private final Double torqueMaximum; //최대 토크
        private final Double mileageCombined; //복합연비
        private final Double mileageCity; //도심연비
        private final Double mileageHighway; //고속도로 연비
        private final Short ratingMileage; //연비등급
        private final Double emissionsCo2; // CO2배출량
        private final Short gradeLowPollution; //저공해등급
        private final Double weightCurb; //공차중량
        private final Integer speedMaximum; //최고속도
        private final Double zeroToSixty; //제로백
        private final Short levelAutomation; //자율주행레벨
        private final String tireSizeFront; //앞 타이어 규격
        private final String tireSizeBack; // 뒷 타이어 규격
        private final BrakeType frontBrake; // 전륜 브레이크
        private final BrakeType backBrake; //후륜 브레이크
        private final Suspension frontSuspensionType;
        private final Suspension backSuspensionType;
        private final PowerSteering powerSteering;
        private final Integer maximumPeople; // 차량 정원
        private final Double fuelEfficiency;
        private final Short chargingMileage; // 충전 주행거리
        private final BatteryType batteryType; //배터리 종류
        private final Double batteryCapacity;
        private final String chargingWay;
        private final Short fastChargeTime; // 급속 충전시간
        private final Short standardChargeTime; //완속 충전시간
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarTrimBuilder(CarRating carRating, CarTrimCreateRequest request) {
            this.nameTrim = request.getNameTrim();
            this.carRating = carRating;
            this.carPrice = request.getCarPrice();
            this.typeFuel = request.getTypeFuel();
            this.typeEngin = request.getTypeEngin();
            this.displacement = request.getDisplacement();
            this.typeGearbox = request.getTypeGearbox();
            this.typeDrive = request.getTypeDrive();
            this.outputHighest = request.getOutputHighest();
            this.torqueMaximum = request.getTorqueMaximum();
            this.mileageCombined = request.getMileageCombined();
            this.mileageCity = request.getMileageCity();
            this.mileageHighway = request.getMileageHighway();
            this.ratingMileage = request.getRatingMileage();
            this.emissionsCo2 = request.getEmissionsCo2();
            this.gradeLowPollution =request.getGradeLowPollution();
            this.weightCurb  = request.getWeightCurb();
            this.speedMaximum = request.getSpeedMaximum();
            this.zeroToSixty = request.getZeroToSixty();
            this.levelAutomation = request.getLevelAutomation();
            this.tireSizeFront = request.getTireSizeFront();
            this.tireSizeBack = request.getTireSizeBack();
            this.frontBrake = request.getFrontBrake();
            this.backBrake = request.getBackBrake();
            this.frontSuspensionType = request.getFrontSuspensionType();
            this.backSuspensionType = request.getBackSuspensionType();
            this.powerSteering = request.getPowerSteering();
            this.maximumPeople = request.getMaximumPeople();
            this.fuelEfficiency = request.getFuelEfficiency();
            this.chargingMileage = request.getChargingMileage();
            this.batteryType = request.getBatteryType();
            this.batteryCapacity = request.getBatteryCapacity();
            this.chargingWay = request.getChargingWay();
            this.fastChargeTime = request.getFastChargeTime();
            this.standardChargeTime = request.getStandardChargeTime();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }

        @Override
        public CarTrim build() {
            return new CarTrim(this);
        }
    }
}
