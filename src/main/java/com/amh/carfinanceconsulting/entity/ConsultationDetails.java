package com.amh.carfinanceconsulting.entity;

import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import com.amh.carfinanceconsulting.model.consultationDetails.ConsultationDetailsCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultationDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String nameClient;

    @Column(nullable = false, length = 20)
    private String phoneNum;

    @JoinColumn(name = "carTrimId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private CarTrim carTrim;

    @Column(nullable = false)
    private LocalDateTime dateConsultRequest; // 신청시간

    private ConsultationDetails(ConsultationDetailsBuilder builder) {
        this.nameClient = builder.nameClient;
        this.phoneNum = builder.phoneNum;
        this.carTrim = builder.carTrim;
        this.dateConsultRequest = builder.dateConsultRequest;
    }

    public static class ConsultationDetailsBuilder implements CommonModelBuilder<ConsultationDetails> {

        private final String nameClient;
        private final String phoneNum;
        private final CarTrim carTrim;
        private final LocalDateTime dateConsultRequest;

        public ConsultationDetailsBuilder(CarTrim carTrim, ConsultationDetailsCreateRequest request) {
            this.nameClient = request.getNameClient();
            this.phoneNum = request.getPhoneNum();
            this.carTrim = carTrim;
            this.dateConsultRequest = LocalDateTime.now();
        }

        @Override
        public ConsultationDetails build() {
            return new ConsultationDetails(this);
        }
    }
}
