package com.amh.carfinanceconsulting.entity;

import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import com.amh.carfinanceconsulting.model.car_rating.CarRatingCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carModelId", nullable = false)
    private CarModel carModel;

    @Column(nullable = false, length = 30)
    private String nameRating;

    @Column(nullable = false, length = 30)
    private String carFuelType;
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private CarRating(CarRatingBuilder builder) {
        this.carModel = builder.carModel;
        this.nameRating = builder.nameRating;
        this.carFuelType = builder.carFuelType;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarRatingBuilder implements CommonModelBuilder<CarRating> {

        private final CarModel carModel;
        private final String nameRating;
        private final String carFuelType;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarRatingBuilder(CarModel carModel, CarRatingCreateRequest request) {
            this.carModel = carModel;
            this.nameRating = request.getNameRating();
            this.carFuelType = request.getCarFuelType();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarRating build() {
            return new CarRating(this);
        }
    }


}
