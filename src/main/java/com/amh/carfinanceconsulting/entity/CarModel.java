package com.amh.carfinanceconsulting.entity;

import com.amh.carfinanceconsulting.enums.CarReleaseStatus;
import com.amh.carfinanceconsulting.enums.CarSize;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import com.amh.carfinanceconsulting.model.car_model.CarManuIdRequest;
import com.amh.carfinanceconsulting.model.car_model.CarModelCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 빌더 안에 넣어줄 것

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carManufacturerId", nullable = false)
    private CarManufacturer carManufacturer; // 제조사

    @Column(nullable = false)
    private Short modelYear; // 연식

    @Column(nullable = false, length = 30)
    private String carModelName; // 자동차 모델 명

    @Column(nullable = false, length = 200)
    private String carModelImageUrl;

    @Column(nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private CarSize carSize;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private CarReleaseStatus carReleaseStatus; // 출시,미정,예정

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

   /* public void putCarManufacturerIdInCarModel(CarManufacturer carManufacturer, CarManuIdRequest request) {
        this.carManufacturer = carManufacturer; // 1번모델의 2번 제조사 바꾼다하면 번호만이 아니라 데이터통채로
        this.dateUpdate = LocalDateTime.now();
    }

    public void putCarModelYear( CarModelCreateRequest request){
        this.modelYear = request.getModelYear();
        this.dateUpdate = LocalDateTime.now();
    }*/

    private CarModel(CarModelBuilder builder) {

        this.carManufacturer = builder.carManufacturer;
        this.carModelName = builder.carModelName;
        this.carModelImageUrl = builder.carModelImageUrl;
        this.carSize = builder.carSize;
        this.carReleaseStatus = builder.carReleaseStatus;
        this.modelYear = builder.modelYear;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class CarModelBuilder implements CommonModelBuilder<CarModel> {

        private final CarManufacturer carManufacturer;
        private final Short modelYear;
        private final String carModelName;// 연식
        private final String carModelImageUrl;
        private final CarSize carSize;
        private final CarReleaseStatus carReleaseStatus; // 출시,미정,예정
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarModelBuilder(CarManufacturer carManufacturer, CarModelCreateRequest request) {
            this.carManufacturer = carManufacturer;
            this.modelYear = request.getModelYear();
            this.carModelName = request.getCarModelName();
            this.carModelImageUrl = request.getCarModelImageUrl();
            this.carSize = request.getCarSize();
            this.carReleaseStatus = request.getCarReleaseStatus();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarModel build() {
            return new CarModel(this);
        }
    }
}
