package com.amh.carfinanceconsulting.entity;

import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import com.amh.carfinanceconsulting.model.car_manufacturer.CarManufacturerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarManufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String nameManufacturer;

    @Column(nullable = false, length = 200)
    private String logoImageUrl; // 로고이미지 추가

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    private CarManufacturer(CarManufacturerBuilder builder) {
        this.nameManufacturer = builder.nameManufacturer;
        this.logoImageUrl = builder.logoImageUrl;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarManufacturerBuilder implements CommonModelBuilder<CarManufacturer> {

        private final String nameManufacturer;

        private final String logoImageUrl;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarManufacturerBuilder(CarManufacturerRequest request)  {
            this.nameManufacturer = request.getNameManufacturer();
            this.logoImageUrl = request.getLogoImageUrl();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarManufacturer build() {
            return new CarManufacturer(this);
        }
    }

}
