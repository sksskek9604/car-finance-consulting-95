package com.amh.carfinanceconsulting.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarInfoDetailRangeItem {
    private Integer minIntValue;

    private Integer maxIntValue;

    private Double minDoubleValue;

    private Double maxDoubleValue; // 세터를 썻기 때문에 안쓰고 싶은건 안들고올 수 있게 됨
    // 빌더를 쓰게되면 계속 호출을 하게되니까.
    // price -> 범위의 뜻인 range로 수정
}
