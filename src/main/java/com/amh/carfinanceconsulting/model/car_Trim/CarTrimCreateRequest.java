package com.amh.carfinanceconsulting.model.car_Trim;

import com.amh.carfinanceconsulting.entity.CarRating;
import com.amh.carfinanceconsulting.enums.car_trim.*;
import io.swagger.models.auth.In;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarTrimCreateRequest {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Long carRatingId;

    @NotNull
    private String nameTrim;

    @NotNull
    private Double carPrice;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private TypeFuel typeFuel;

    @NotNull
    @Length(max = 50)
    private String typeEngin; //엔진형식

    @NotNull
    private Integer displacement; //배기량

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private TypeGearBox typeGearbox; //변속기 종류

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private TypeDrive typeDrive; // 구동 방식

    @NotNull
    private Integer outputHighest; // 최고 출력

    @NotNull
    private Double torqueMaximum; //최대 토크

    @NotNull
    private Double mileageCombined; //복합연비

    @NotNull
    private Double mileageCity; //도심연비

    @NotNull
    private Double mileageHighway; //고속도로 연비

    @NotNull
    private Short ratingMileage; //연비등급

    @NotNull
    private Double emissionsCo2; // CO2배출량

    @NotNull
    private Short gradeLowPollution; //저공해등급

    @NotNull
    private Double weightCurb; //공차중량

    @NotNull
    private Integer speedMaximum; //최고속도

    @NotNull
    private Double zeroToSixty; //제로백

    @NotNull
    private Short levelAutomation; //자율주행레벨

    @NotNull
    @Length(max = 20)
    private String tireSizeFront; //앞 타이어 규격

    @NotNull
    @Length(max = 20)
    private String tireSizeBack; // 뒷 타이어 규격

    @NotNull
    @Enumerated(EnumType.STRING)
    private BrakeType frontBrake; // 전륜 브레이크

    @NotNull
    @Enumerated(EnumType.STRING)
    private BrakeType backBrake; //후륜 브레이크

    @NotNull
    @Enumerated(EnumType.STRING)
    private Suspension frontSuspensionType;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Suspension backSuspensionType;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PowerSteering powerSteering;

    @NotNull
    private Integer maximumPeople; // 차량 정원

    @NotNull
    private Double fuelEfficiency;

    @NotNull
    private Short chargingMileage; // 충전 주행거리

    @NotNull
    @Enumerated(EnumType.STRING)
    private BatteryType batteryType; //배터리 종류

    @NotNull
    private Double batteryCapacity;

    @NotNull
    private String chargingWay;

    @NotNull
    private Short fastChargeTime; // 급속 충전시간

    @NotNull
    private Short standardChargeTime; //완속 충전시간
}
