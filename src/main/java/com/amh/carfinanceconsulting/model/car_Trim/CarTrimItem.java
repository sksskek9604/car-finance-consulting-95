package com.amh.carfinanceconsulting.model.car_Trim;

import com.amh.carfinanceconsulting.entity.CarRating;
import com.amh.carfinanceconsulting.enums.car_trim.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarTrimItem {

    private Long id;

    private String nameTrim;

    private Double carPrice; //가격, 만원단위니까 0도 수 만큼 많이 붙여야함

    private CarRating carRating;

    private TypeFuel typeFuel;

    private String typeEngin; //엔진형식

    private Integer displacement; //배기량

    private TypeGearBox typeGearbox; //변속기 종류

    private TypeDrive typeDrive; // 구동 방식

    private Integer outputHighest; // 최고 출력

    private Float torqueMaximum; //최대 토크

    private Float mileageCombined; //복합연비

    private Float mileageCity; //도심연비

    private Float mileageHighway; //고속도로 연비

    private Short ratingMileage; //연비등급

    private Float emissionsCo2; // CO2배출량

    private Short gradeLowPollution; //저공해등급

    private Float weightCurb; //공차중량

    private Float speedMaximum; //최고속도

    private Float zeroToSixty; //제로백

    private Short levelAutomation; //자율주행레벨

    private String tireSizeFront; //앞 타이어 규격

    private String tireSizeBack; // 뒷 타이어 규격

    private BrakeType frontBrake; // 전륜 브레이크

    private BrakeType backBrake; //후륜 브레이크

    private Suspension frontSuspensionType;

    private Suspension backSuspensionType;

    private PowerSteering powerSteering;

    private Short maximumPeople; // 차량 정원

    private Float fuelEfficiency;

    private Short chargingMileage; // 충전 주행거리

    private BatteryType batteryType; //배터리 종류

    private Float batteryCapacity;

    private String chargingWay;

    private Short fastChargeTime; // 급속 충전시간

    private Short standardChargeTime; //완속 충전시간

    private LocalDateTime dateCreate;

    private LocalDateTime dateUpdate;
}
