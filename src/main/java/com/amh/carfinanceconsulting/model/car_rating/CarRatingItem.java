package com.amh.carfinanceconsulting.model.car_rating;

import com.amh.carfinanceconsulting.entity.CarRating;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarRatingItem {

    private Long id;
    private String nameRating;
    private Long carModel;

    private Short modelYear;
    private LocalDateTime dateCreate;
    private LocalDateTime dateUpdate;

    private CarRatingItem(CarRatingItemBuilder builder) {
        this.id = builder.id;
        this.nameRating = builder.nameRating;
        this.carModel = builder.carModel;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class CarRatingItemBuilder implements CommonModelBuilder<CarRatingItem> {
        private final Long id;
        private final String nameRating;
        private final Long carModel;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarRatingItemBuilder(CarRating carRating) {
            this.id = carRating.getId();
            this.nameRating = carRating.getNameRating();
            this.carModel = carRating.getCarModel().getId();
            this.dateCreate = carRating.getDateCreate();
            this.dateUpdate = carRating.getDateUpdate();
        }

        @Override
        public CarRatingItem build() {
            return new CarRatingItem(this);
        }
    }
}
