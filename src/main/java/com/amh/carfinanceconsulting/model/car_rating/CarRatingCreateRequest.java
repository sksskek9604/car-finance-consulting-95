package com.amh.carfinanceconsulting.model.car_rating;

import com.amh.carfinanceconsulting.entity.CarModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarRatingCreateRequest {

    @NotNull
    private Long carModelId;

    @NotNull
    @Length(max = 30)
    private String nameRating;

    @NotNull
    @Length(max = 30)
    private String carFuelType;


}
