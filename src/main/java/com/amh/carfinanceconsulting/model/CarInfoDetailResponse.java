package com.amh.carfinanceconsulting.model;

import com.amh.carfinanceconsulting.entity.CarModel;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarInfoDetailResponse {

    private String carFullName;
    private String carReleaseStatusName; // 제조사 옆에 new인지 무엇인지 시판
    private CarInfoDetailRangeItem priceItem;
    private String carType; // 차량 외장
    private List<String> carFuelTypes; // 차량 연료 종류 (콤마구분)
    private CarInfoDetailRangeItem displacementItem; // 배기량 최소~최대
    private CarInfoDetailRangeItem fuelEfficiencyItem; // 연비(=mileage) 최소~최대
    private CarInfoDetailRangeItem maximumPeople; // 정원 최소~최대

    private CarInfoDetailResponse(CarInfoDetailResponseBuilder builder) {
        this.carFullName = builder.carFullName;
        this.carReleaseStatusName = builder.carReleaseStatusName;
        this.priceItem = builder.priceItem;
        this.carType = builder.carType;
        this.carFuelTypes = builder.carFuelTypes;
        this.displacementItem = builder.displacementItem;
        this.fuelEfficiencyItem = builder.fuelEfficiencyItem;
        this.maximumPeople = builder.maximumPeople;
    }

    public static class CarInfoDetailResponseBuilder implements CommonModelBuilder<CarInfoDetailResponse> {

        private final String carFullName;
        private final String carReleaseStatusName; // 제조사 옆에 new인지 무엇인지 시판
        private final CarInfoDetailRangeItem priceItem;
        private final String carType; // 외장
        private final List<String> carFuelTypes; // 콤마구분
        private final CarInfoDetailRangeItem displacementItem; // 배기량
        private final CarInfoDetailRangeItem fuelEfficiencyItem; // 연비
        private final CarInfoDetailRangeItem maximumPeople;

        public CarInfoDetailResponseBuilder(
                CarModel carModel,
                Double minPrice,
                Double maxPrice,
                List<String> carFuelTypes,
                Integer minDisplacement,
                Integer maxDisplacement,
                Double minFuelEfficiencyItem, //최소 배기량
                Double maxFuelEfficiencyItem, // 최대 배기량
                Integer minMaximumPeople,
                Integer maxMaximumPeople
        ) {
            this.carFullName = carModel.getCarManufacturer().getNameManufacturer() + " " + carModel.getCarModelName();
            this.carReleaseStatusName = carModel.getCarReleaseStatus().getName();

            CarInfoDetailRangeItem priceItem = new CarInfoDetailRangeItem();
            // 이전에 해봤던 setter문법
            priceItem.setMinDoubleValue(minPrice);
            priceItem.setMaxDoubleValue(maxPrice); // 4개 중의 2개만 사용할 수 있게 꺼내온 것
            this.priceItem = priceItem; // 54번째줄에 있는 priceItem을 바로 써준 것.

            this.carType = carModel.getCarModelName() + " (" + carModel.getCarSize().getName() + ") "; // 외장에 들어갈 내용
            this.carFuelTypes = carFuelTypes;

            CarInfoDetailRangeItem displacementItem = new CarInfoDetailRangeItem();
            displacementItem.setMinIntValue(minDisplacement);
            displacementItem.setMaxIntValue(maxDisplacement);
            this.displacementItem = displacementItem;

            CarInfoDetailRangeItem fuelEfficiencyItem = new CarInfoDetailRangeItem();
            fuelEfficiencyItem.setMinDoubleValue(minFuelEfficiencyItem);
            fuelEfficiencyItem.setMaxDoubleValue(maxFuelEfficiencyItem);
            this.fuelEfficiencyItem = fuelEfficiencyItem;

            CarInfoDetailRangeItem maximumPeople = new CarInfoDetailRangeItem();
            maximumPeople.setMinIntValue(minMaximumPeople);
            maximumPeople.setMaxIntValue(maxMaximumPeople);
            this.maximumPeople = maximumPeople;

        }

        @Override
        public CarInfoDetailResponse build() {
            return new CarInfoDetailResponse(this);
        }
    }
}



