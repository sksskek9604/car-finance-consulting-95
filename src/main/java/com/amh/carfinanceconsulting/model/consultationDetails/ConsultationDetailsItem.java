package com.amh.carfinanceconsulting.model.consultationDetails;

import com.amh.carfinanceconsulting.entity.ConsultationDetails;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultationDetailsItem {
    //트림에서 보여줄 것만 고를 것 + 기간 필터 사용
    private Long ConsultationDetailsId; // 상담내역 시퀀스
    private String clientFullInfo;
    private LocalDateTime dateConsultRequest;
    private Long carTrimId; // 트림 시퀀스
    private String CarFullInfo; // 자동차 제조사 + 모델명 + 등급명 + 트림명 + 가격

    private ConsultationDetailsItem(ConsultationDetailsItemBuilder builder) {
        this.ConsultationDetailsId = builder.ConsultationDetailsId;
        this.clientFullInfo = builder.clientFullInfo;
        this.dateConsultRequest = builder.dateConsultRequest;
        this.carTrimId = builder.carTrimId;
        this.CarFullInfo = builder.CarFullInfo;
    }


    public static class ConsultationDetailsItemBuilder implements CommonModelBuilder<ConsultationDetailsItem> {

        private final Long ConsultationDetailsId; // 상담내역 시퀀스
        private final String clientFullInfo;
        private final LocalDateTime dateConsultRequest;
        private final Long carTrimId; // 트림 시퀀스
        private final String CarFullInfo; // 자동차 제조사 + 모델명 + 등급명 + 트림명 + 가격

        public ConsultationDetailsItemBuilder(ConsultationDetails consultationDetails) {
            this.ConsultationDetailsId = consultationDetails.getId();
            this.clientFullInfo = consultationDetails.getNameClient() + " " + consultationDetails.getPhoneNum();
            this.dateConsultRequest = LocalDateTime.now();
            this.carTrimId = consultationDetails.getCarTrim().getId();
            this.CarFullInfo = consultationDetails.getCarTrim().getCarRating().getCarModel().getCarManufacturer().getNameManufacturer()
                    + " " + consultationDetails.getCarTrim().getCarRating().getCarModel().getCarModelName() + " " + consultationDetails.getCarTrim().getCarRating().getNameRating()
                    + " " + consultationDetails.getCarTrim().getNameTrim() + " 가격: " + consultationDetails.getCarTrim().getCarPrice();
        }

        @Override
        public ConsultationDetailsItem build() {
            return new ConsultationDetailsItem(this);
        }
    }

}

