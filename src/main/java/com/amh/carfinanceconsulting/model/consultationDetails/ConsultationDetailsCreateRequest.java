package com.amh.carfinanceconsulting.model.consultationDetails;

import com.amh.carfinanceconsulting.entity.CarTrim;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ConsultationDetailsCreateRequest {

    @NotNull
    private Long carTrimId;

    @NotNull
    @Length(max = 20)
    private String nameClient;

    @NotNull
    @Length(max = 20)
    private String phoneNum;
}
