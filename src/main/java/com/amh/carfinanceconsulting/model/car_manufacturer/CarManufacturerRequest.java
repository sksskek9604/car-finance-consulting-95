package com.amh.carfinanceconsulting.model.car_manufacturer;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarManufacturerRequest {

    @NotNull
    @Length(max = 30)
    private String nameManufacturer;

    @NotNull
    @Length(max = 200)
    private String logoImageUrl;

}
