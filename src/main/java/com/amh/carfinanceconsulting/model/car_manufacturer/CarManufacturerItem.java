package com.amh.carfinanceconsulting.model.car_manufacturer;

import com.amh.carfinanceconsulting.entity.CarManufacturer;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarManufacturerItem {

    private Long id;

    private String nameManufacturer;

    private String logoImageUrl;

    private LocalDateTime dateCreate;

    private LocalDateTime dateUpdate;

    private CarManufacturerItem(CarManufacturerItemBuilder builder) {
        this.id = builder.id;
        this.nameManufacturer = builder.nameManufacturer;
        this.logoImageUrl = builder.logoImageUrl;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarManufacturerItemBuilder implements CommonModelBuilder<CarManufacturerItem> {
        private final Long id;
        private final String nameManufacturer;

        private final String logoImageUrl;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarManufacturerItemBuilder(CarManufacturer carManufacturer) {
            this.id = carManufacturer.getId();
            this.nameManufacturer = carManufacturer.getNameManufacturer();
            this.logoImageUrl = carManufacturer.getLogoImageUrl();
            this.dateCreate = carManufacturer.getDateCreate();
            this.dateUpdate = carManufacturer.getDateUpdate();
        }
        @Override
        public CarManufacturerItem build() {
            return new CarManufacturerItem(this);
        }
    }
}
