package com.amh.carfinanceconsulting.model.car_model;

import com.amh.carfinanceconsulting.entity.CarManufacturer;
import com.amh.carfinanceconsulting.enums.CarReleaseStatus;
import com.amh.carfinanceconsulting.enums.CarSize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelCreateRequest {

    @NotNull
    private Long carManufacturerId;

    @NotNull
    private Short modelYear; // 연식

    @NotNull
    @Length(max = 30)
    private String carModelName; // 자동차 모델 명

    @NotNull
    @Length(max = 200)
    private String carModelImageUrl;

    @NotNull
    @Length(max = 15)
    private CarSize carSize;

    @NotNull
    private CarReleaseStatus carReleaseStatus; // 출시,미정,예정




}
