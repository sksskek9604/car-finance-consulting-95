package com.amh.carfinanceconsulting.model.car_model;

import com.amh.carfinanceconsulting.entity.CarModel;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelItem {

    private Long id;

    private Short modelYear; // 연식

    private Long carManufacturerId;

    private String nameManufacturer;

    private LocalDateTime dateCreate;

    private LocalDateTime dateUpdate;

    private CarModelItem(CarModelItemBuilder builder) {
        this.id = builder.id;
        this.modelYear = builder.modelYear;
        this.carManufacturerId = builder.carManufacturerId;
        this.nameManufacturer = builder.nameManufacturer;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarModelItemBuilder implements CommonModelBuilder<CarModelItem> {

        private final Long id;
        private final Short modelYear; // 연식
        private final Long carManufacturerId;
        private final String nameManufacturer;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarModelItemBuilder(CarModel carModel) {
            this.id = carModel.getId();
            this.modelYear = carModel.getModelYear();
            this.carManufacturerId = carModel.getCarManufacturer().getId();
            this.nameManufacturer = carModel.getCarManufacturer().getNameManufacturer();
            this.dateCreate = carModel.getDateCreate();
            this.dateUpdate = carModel.getDateUpdate();
        }

        @Override
        public CarModelItem build() {
            return null;
        }
    }
}
