package com.amh.carfinanceconsulting.model.car_model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CarManuIdRequest {

    @NotNull
    private Long carManufacturerId;

    @NotNull
    private LocalDateTime dateUpdate;
}
