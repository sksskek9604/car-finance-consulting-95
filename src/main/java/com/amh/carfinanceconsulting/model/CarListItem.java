package com.amh.carfinanceconsulting.model;

import com.amh.carfinanceconsulting.entity.CarModel;
import com.amh.carfinanceconsulting.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarListItem {
    private String carReleaseStatusName;

    private String carFullName;

    private CarListItem(CarListItemBuilder builder) {
        this.carReleaseStatusName = builder.carReleaseStatusName;
        this.carFullName = builder.carFullName;
    }

    public static class CarListItemBuilder implements CommonModelBuilder<CarListItem> {

        private final String carReleaseStatusName;

        private final String carFullName;
        //모델은 이미 제조사를 알고 있다. 그러니 모델을 들고온다.
        public CarListItemBuilder(CarModel carModel) {
            this.carReleaseStatusName = carModel.getCarReleaseStatus().getName();
            this.carFullName = carModel.getCarManufacturer().getNameManufacturer() + " " + carModel.getCarModelName();

        }

        @Override
        public CarListItem build() {
            return new CarListItem(this);
        }
    }
}
