package com.amh.carfinanceconsulting.enums.car_trim;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BrakeType {
    DISC_VENTILATED("V디스크"),
    DISC_SOLID("솔리드 디스크"),
    DRUM_BRAKE("드럼 브레이크");

    private final String name;
}
