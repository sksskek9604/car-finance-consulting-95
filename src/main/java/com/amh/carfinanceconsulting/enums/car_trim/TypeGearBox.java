package com.amh.carfinanceconsulting.enums.car_trim;

import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public enum TypeGearBox {
    MT("수동변속기"),
    AT("자동변속기"),
    AMT("자동화 수동변속기"),
    DCT("듀얼 클러치 변속기"),
    CVT("무단 변속기");

    private final String name;
}
