package com.amh.carfinanceconsulting.enums.car_trim;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypeDrive {
    FF("FF"),
    FR("프론트")
    , MR("미드쉽")
    , RR("리얼")
    , WD4("4개바퀴");

    private final String name;
}
