package com.amh.carfinanceconsulting.enums.car_trim;

import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public enum TypeFuel {
    GASOLIN("가솔린")
    , DIESEL("디젤")
    , LPG("LPG")
    , HYBRID("하이브리드")
    , ELECTRIC("전기")
    , HYDROGEN("수소차")
    ;

    private final String name;
}