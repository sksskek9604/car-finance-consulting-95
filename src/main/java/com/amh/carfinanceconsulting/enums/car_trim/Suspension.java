package com.amh.carfinanceconsulting.enums.car_trim;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Suspension {
    DOUBLE_WISHBONE("더블 위시본"),
    MACPHERSON_STRUT("맥퍼슨 스트럿"),
    MULTI_LINK("멀티링크"),
    RIGID_AXLE("리지드 액슬"),
    SWING_ARM("스윙암"),
    TORSION_BEAM("토션빔");

    private final String name;
}
