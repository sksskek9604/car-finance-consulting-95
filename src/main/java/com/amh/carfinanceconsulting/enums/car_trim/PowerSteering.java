package com.amh.carfinanceconsulting.enums.car_trim;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PowerSteering {

    HPS("유압식"),
    EPS("전동식");

    private final String name;
}
