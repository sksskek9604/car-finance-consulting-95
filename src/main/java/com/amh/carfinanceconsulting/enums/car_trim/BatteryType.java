package com.amh.carfinanceconsulting.enums.car_trim;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BatteryType {
    LITHIUM_ION("리튬 이온");

    private final String name;

}
