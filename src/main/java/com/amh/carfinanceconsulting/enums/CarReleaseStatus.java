package com.amh.carfinanceconsulting.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarReleaseStatus {

    ON_SALE("시판"),
    RELEASE("출시"),
    PLANNING("예정"),
    YET("미정");

    private final String name;
}
