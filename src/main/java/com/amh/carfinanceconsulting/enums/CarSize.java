package com.amh.carfinanceconsulting.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarSize {

    COMPACT_CAR("소형차"),
    MEDIUM_SEDAN("중형차"),
    FULL_SIZE_SEDAN("대형차"),
    TRUCK("트럭"),
    SUB_COMPACT_CAR("경차");

    private final String name;
}
